![alt text](http://www.mustangx.org/mx-imgz/logo/MX_LOGO_v1.1_CMS-black_300-159.png "MustangX Logo")

[![build status](https://gitlab.com/MustangX/MustangX/badges/master/build.svg)](https://gitlab.com/MustangX/wisaka/commits/master)
[![coverage report](https://gitlab.com/MustangX/MustangX/badges/master/coverage.svg)](https://gitlab.com/wisaka/MustangX/commits/master)



# Codename:     Wisaka

Wisaka is the transformer god of [the great Algonquin people](https://en.wikipedia.org/wiki/Algonquian_peoples) of the plains. 
Wisaka is a friend to humans, even if he likes to tease them sometimes. 
Many Algonquin myths show Wisaka creating the human race in one of many ways. 
This name could work for a number of horses, particularly one you see as a really good friend or one you expect to become one.

The exerpt above was taken from [this webpage]().

Well MustangX CMS expects to become a really good friend to you. That's why [**Wisaka**](http://www.native-languages.org/wisaka.htm) is the chosen codename.


Building a _**CMS from scratch**_ ~~seems to be~~ *IS* a radical thing to do, with so many Content
Management Systems available. But I have lost some of my PHP & MySQL skills, last
used php4 and MySql3. So this will be an experience to brush up on this.

I'm not finding a CMS, that I like use.

----

**BUUUUTTTTT**, we are going to build a system (_almost_) _**from scratch**_. We will
use Symfony 4 and use as many components and/or bundles as needed and see the System grow!.


Adding general website neccesities to the base, the CORE.

Building  modules, plugins, themes, skins, etc. - whatever we will call them. (Hopefully - not as bundles)



## Planned **CORE** FEATURES

* Integreted _PAGES_ with _Catagories_
  * Create pages that do not change often. Such as:
    * Legal Pages
    * Tutorials
    * About Us

* EU Cookie Law Compliance
  * No theme dependance

* Captcha and/or reCaptcha

* Contact Page

* Update/Upgrade Method
  * Something like WP has (drupal is ok too)

* Bootstrap

* JQuery



## Planned Requirements

* PHP 7+
* MySQLi (pdo-mysqli prefered)
* Webserver
   * Apache2 or nGinX (maybe others)
   * PHP 7
   * MySQL

## Documentation

During the development phases that are HUGE changes, will have most of it's
documentation [here in our wiki](https://gitlab.com/MustangX/wisaka/wikis/home)

Once we start using a release, even if it's still in testing status, are at [the projects website](http://mustangx.org)


## License

MIT License


### Misc

We may add some snippets that may be usefull for the project. This will stay [here in the snippets area](https://gitlab.com/MustangX/wisaka/snippets),

in case a _core, module_ or _theme_ developer needs it.




#### How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/wisaka/wisaka.git`

**IMPORTANT!!!** Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd wisaka`



```bash
composer install --with-dependencies
```

answer any questions and it will or may end with errors message about an unkown databank. Fix that error with this:

```bash
php bin/console doctrine:database:create
````

And then to make sure we got erverything the first time, we run the _install_ command again:

```bash
composer install
```


Then Create the database schema

```bash
php bin/console doctrine:schema:create
```


And now update your database:

```bash
php bin/console doctrine:schema:update --force
```


Then create your :Admin User_ (Replace _"Admin"_ with your adminuser name):

```bash
php bin/console fos:user:create Admin --super-admin
```


#### This is needed for ckeditor to function!
```bash
php bin/console ckeditor:install
```

And then we run this to update everything:

```bash
composer update --with-dependencies
```

#### If you want the example date do this:

```bash
php bin/console assets:install public --symlink

php bin/console doctrine:fixtures:load
```

_I HAVE NOT TESTED THIS YET. If you do please post an issue and let us know._
